# Polar Express

This is a prime example of a pipeline that could be used for any project.
I have made sure that if there changes made to a branch other than the master branch they can not be deployed to staging or prod.
Plus even if changes are made to other branches the code is still checked for linting, quality, and tests. 
If they pass through all of these stages then they still need to be put through a merge request and approved by an admin developer.
Once approved they will be merged into master and deployed to staging. If the admin is satisfied with staging then they have to manually deploy to production. Each of these stages of devops allows the develop to continually create effective and quality code without ruining a repo. 
