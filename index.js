import React from "react";
import ReactDom from "react-dom";
import App from "./App"
// const App = () => <h1>Welcome to Awesome!</h1>;

ReactDom.render(<App />, document.getElementById("app"));
