import React from 'react';
// import {Header, Button} from './components'
function App() {
  return (
    <>
      {/* add data-testid to target content div */}
      <div data-testid="content"><h1>Welcome to Awesome Town!</h1></div>
    </>
  );
}
export default App;